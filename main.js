const cheerio = require("cheerio");
const https = require("https");

const GITLAB_FEED_XML = "https://gitlab.com/rubidium-dev.atom";
const BASE_HTML_DOC = "<!DOCTYPE html><head><title>GitLab feed</title><link rel=\"stylesheet\" href=\"/assets/css/styles.css\"></head><body></body></html>";

https.get(GITLAB_FEED_XML, (response) => {
    let data = "";
    response.on("data", (d) => {
        data += d;
    });
    response.on("end", () => {
        process_xml(data);
    });
}).on("error", (e) => {
    console.error(e);
});;

function process_xml(xml) {
    const $ = cheerio.load(xml, { xmlMode: true });
    const out = cheerio.load(BASE_HTML_DOC);
    let root = cheerio("<div></div>");
    root.addClass("gitlab_feed");
    out("body").append(root);
    $("entry")
        .filter((idx, elem) => {
            return $("link[href^='https://gitlab.com/rubidium-dev/www']", elem).length === 0;
        })
        .slice(0, 5).each((idx, entry) => {
        let html = cheerio("<div></div>");
        html.addClass("entry");

        let thumbnail = $("media\\:thumbnail", entry);
        let thumbnail_width = thumbnail.attr("width");
        let thumbnail_height = thumbnail.attr("height");
        let thumbnail_url = thumbnail.attr("url");
        let img = cheerio("<img>");
        img.addClass("thumbnail");
        img.attr("width", thumbnail_width);
        img.attr("height", thumbnail_height);
        img.attr("src", thumbnail_url);
        html.append(img);

        let title = $("title", entry).text();
        let h4 = cheerio("<h4></h4>");
        h4.text(title);
        html.append(h4);

        let summary = $("summary", entry);
        summary.children().first().removeAttr("xmlns").addClass("summary");
        $("a", summary).attr("target", "_blank");
        html.append(summary.children());

        root.append(html);
    });
    console.log(out.html());
}